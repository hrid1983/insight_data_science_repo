Please follow the below instructions to test the code.
-------------------------------------------------------
1. Checkout the project to a folder.
2. Place the input file named 'complains.csv' inside the './input' folder which needs to be processed.Please note the input filename is case-sensitive.
3. Execute the shell script 'sh run.sh'. After successful execution of the script, the 'report.csv' will be created inside the './output' directory.

Assumptions:
------------
1. Current code expects 'Date Received' column in "yyyy-MM-dd" format. eg. '2019-09-24' is valid date, '2019/09/24' is not a valid date in expected format.
   Incase the requirmemts changes in future, to accept multiple date fromats, simple changes can be made to the code to support it.

Test Cases:
-----------
1. test_1 - The input file inside './insight_testsuite/test_1/input/complains.csv' contains the sample test file. 
            The generated report for this file is present in './insight_testsuite/test_1/output/report.csv' file.
2. test_2_duplicate_data - The input file inside './insight_testsuite/test_2_duplicate_data/input/complains.csv' contains the input file with duplicated data.
            The generated report for this file is present in './insight_testsuite/test_2_duplicate_data/output/report.csv' file.
3. test_3_incorrect_date_format - The input file inside './insight_testsuite/test_3_incorrect_date_format/input/complains.csv' contains all the records with incorrect date format.
            The generated empty report for this file is present in './insight_testsuite/test_3_incorrect_date_format/output/report.csv' file as all the reconds in the input file are invalid records.

Future Enhancements:
----------------------
Pleases note the sample test file complains.csv which has close to 2.7 million records, was successfully processed by this code in 30 sec.
We can considerably bring down the processing time by using multi-threading techniques using Execcutor framework.
Due to lack of time, the current code is single threaded.
  
Please follow the below instructions to test the code.
-------------------------------------------------------
1. Checkout the project to a folder.
2. Place the input file named 'complains.csv' inside the './input' folder which needs to be processed.Please note the input filename is case-sensitive.
3. Execute the shell script 'sh run.sh'. After successful execution of the script, the 'report.csv' will be created inside the './output' directory.

Assumptions:
------------
1. Current code expects 'Date Received' column in "yyyy-MM-dd" format. eg. '2019-09-24' is valid date, '2019/09/24' is not a valid date in expected format.
   Incase the requirmemts changes in future, to accept multiple date fromats, simple changes can be made to the code to support it.

Test Cases:
-----------
1. test_1 - The input file inside './insight_testsuite/test_1/input/complains.csv' contains the sample test file. 
            The generated report for this file is present in './insight_testsuite/test_1/output/report.csv' file.
2. test_2_duplicate_data - The input file inside './insight_testsuite/test_2_duplicate_data/input/complains.csv' contains the input file with duplicated data.
            The generated report for this file is present in './insight_testsuite/test_2_duplicate_data/output/report.csv' file.
3. test_3_incorrect_date_format - The input file inside './insight_testsuite/test_3_incorrect_date_format/input/complains.csv' contains all the records with incorrect date format.
            The generated empty report for this file is present in './insight_testsuite/test_3_incorrect_date_format/output/report.csv' file as all the reconds in the input file are invalid records.

Future Enhancements:
----------------------
Pleases note the sample test file complains.csv which has close to 2.7 million records, was successfully processed by this code in 30 sec.
We can considerably bring down the processing time by using multi-threading techniques using Execcutor framework.
Due to lack of time, the current code is single threaded.
  
