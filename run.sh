#!/usr/bin/ksh



echo "Start of script run.sh. Start of Java program to generate report by reading the complaints.csv file from ./input/complaints.csv"

echo "compiling java code ProcessConsumerComplaint.java " 
cd src
javac ProcessConsumerComplaint_1.java

echo "Before Executing java code ProcessConsumerComplaint.java " 
now=$(date +"%T")
echo "Start time : $now"
java ProcessConsumerComplaint_1 "../input/complaints.csv" "../output/report.csv"
now=$(date +"%T")
echo "End time : $now"

echo "After successfully executing java code ProcessConsumerComplaint. Please check the generatd report at './insight_data_science/output/report.csv' " 
cd ..

echo "End of script run.sh "